<?php

namespace Database\Seeders;
use App\Models\noticia;
use Illuminate\Database\Seeder;

class noticiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        noticia::factory(100)->create();
    }
}
