<?php

namespace Database\Factories;

use App\Models\noticia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class noticiaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = noticia::class;
    public function definition()
    {
        return [
            'titulo' => $this->faker->sentence(),
            'autor' => $this->faker->sentence(),
            'fecha_publicacion' => $this->faker->date(),
            'contenido' => $this->faker->paragraph(),
            'imagen' => $this->faker->sentence()
        ];
    }
}
