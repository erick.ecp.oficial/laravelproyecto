<?php

namespace Database\Factories;

use App\Models\categoria;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class categoriaFactory extends Factory
{
    /**
     * Define the model's default state.

     *
     * @return array
     */
    protected $model = categoria::class;

    public function definition()
    {
        return [
            'descripcion' => $this->faker->word(),
        ];
    }
}
