<?php

namespace App\Http\Controllers;

use App\Models\categoria;
use App\Models\noticia;
use Illuminate\Http\Request;

class RelacionController extends Controller
{
    public function index(){
        /* $noticia = noticia::find(1);
        $categorias = categoria::find(7); */
        $categorias = categoria::all();
        return view('categorias.index', compact('categorias'));

    }
    public function show($id){

        $categorias = categoria::where('id', $id)->firstOrFail();
        $categorias->setRelation('noticias', $categorias->noticias()->paginate(5));

        return view('categorias.show', compact('categorias' ));

    }
}
