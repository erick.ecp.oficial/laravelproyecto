<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class noticia extends Model
{
    use HasFactory;
    protected $fillable = ['titulo', 'autor', 'fecha_publicacion', 'contenido', 'imagen'];


    public function categorias()
    {
        return $this->belongsToMany('App\Models\categoria');
    }

}
