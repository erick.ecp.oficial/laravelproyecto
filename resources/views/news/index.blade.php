@extends('layouts.base');
@section('content')
<h1 class="PB-4">
    NOTICIAS INTERNACIONALES
</h1>

<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Fecha de publicacion</th>
            </tr>
        </thead>
        <tbody>
    @foreach($noticias as $noticia)
            <tr>
                <td scope="row"> {{$noticia->id}}</td>
                <td><a href="{{route('news.show', $noticia->id)}}">{{ $noticia->titulo }}</a>  </td>
                <td>{{$noticia->fecha_publicacion}}</td>
            </tr>
    @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-end">
        {!! $noticias->links() !!}
    </div>
    
</div>

@endsection