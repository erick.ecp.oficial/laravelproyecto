
@extends('layouts.base')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1 class="text-start">{{$noticia->titulo}}</h1>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card p-4">
                <div class="row">
                    <div class="col-4">
                    <img class="card-img-top" src="{{$noticia->imagen}}" alt="">
                    </div>
                    <div class="col-8">
                    <p class="card-text">{{ $noticia->contenido }}</p>
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="card-text text-end">{{ $noticia->autor }}</p>
                    <p class="card-text text-end text-info">{{ $noticia->fecha_publicacion }}</p>
                </div>
            </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>



@endsection