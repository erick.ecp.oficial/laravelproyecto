@extends('layouts.base');
@section('content')
<h1 class="PB-4">
    NOTICIAS INTERNACIONALES
</h1>

<div class="row">
    @foreach($noticias as $noticia)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{$noticia->imagen}}" alt="">
            <div class="card-body">
                <h4 class="card-title"><a href="{{route('noticias.show', $noticia->id)}}">{{ $noticia->titulo }}</a></h4>
                <p class="card-text">{{ $noticia->autor }}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection