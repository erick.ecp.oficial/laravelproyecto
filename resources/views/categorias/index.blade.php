@extends('layouts.base');
@section('content')
<h1 class="PB-4">
    CATEGORIAS
</h1>
<ul class="list-group">
    @foreach($categorias as $categoria)
        <li class="list-group-item"> <strong>{{$categoria->id }} - </strong>
             <a href="{{route('categorias.show', $categoria->id)}}">{{$categoria->descripcion }}</a></li>
    @endforeach
  </ul>
  

@endsection